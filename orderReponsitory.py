from dbConnect import *
import json
import sqlite3

db = SQLAlchemy(app)


class Order(db.Model):
    __tablename__ = 'orders'
    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String(255), nullable=False)
    userId = db.Column(db.Integer, nullable=False)
    total = db.Column(db.Integer, nullable=False)
    createTime = db.Column(db.Integer, nullable=False)

    def json(self):
        return {'id': self.id, 'code': self.code,
                'userId': self.userId, 'total': self.total, 'createTime': self.createTime}

    def add_order(_code, _userId, _total, _createTime):
        new_order = Order(code=_code, userId=_userId, total=_total, createTime=_createTime)
        db.session.add(new_order)
        db.session.commit()

    def get_all_order():
        return [Order.json(orders) for orders in Order.query.all()]

    def get_order(_id):
        return [Order.json(Order.query.filter_by(id=_id).first())]

    def update_order(_id, _code, _userId, _total, _createTime):
        order_to_update = Order.query.filter_by(id=_id).first()
        order_to_update.code = _code
        order_to_update.userId = _userId
        order_to_update.total = _total
        order_to_update.createTime = _createTime
        db.session.commit()

    def delete_order(_id):
        Order.query.filter_by(id=_id).delete()
        db.session.commit()

    def add_order1(_code, _userId, _total, _createTime):
        db1 = sqlite3.connect('FruitStore.db')
        sql = ''' INSERT INTO orders(code, userId, total, createTime)
                     VALUES(?,?,?,?) '''
        cur = db1.cursor()
        task = (_code, _userId, _total, _createTime)
        cur.execute(sql, task)

        return cur.lastrowid
