from orderReponsitory import *
from orderDetailReponsitory import *


# Created by: Manhnd
# Create date: 14h - 14/03/2021
# Description: Get all orders
@app.route('/orders', methods=['GET'])
def get_orders():
    return jsonify({'Order': Order.get_all_order()})


# Created by: Manhnd
# Create date: 14h20 - 14/03/2021
# Description: Get by id
@app.route('/orders/<int:id>', methods=['GET'])
def get_order_by_id(id):
    return_value = Order.get_order(id)
    return jsonify(return_value)


# Created by: Manhnd
# Create date: 14h - 14/03/2021
# Description: Add order
@app.route('/orders', methods=['POST'])
def add_order():
    request_data = request.get_json()
    Order.add_order(request_data["code"], request_data["userId"],
                    request_data["total"], request_data["createTime"])
    response = Response("order added", status=200, mimetype='application/json')
    return response


# Created by: Manhnd
# Create date: 14h - 14/03/2021
# Description: Update order
@app.route('/orders/<int:id>', methods=['PUT'])
def update_order(id):
    request_data = request.get_json()
    Order.update_order(id, request_data['code'], request_data['userId'], request_data['total'],
                       request_data["createTime"])
    response = Response("Order Updated", status=200, mimetype='application/json')
    return response


# Created by: Manhnd
# Create date: 14h - 14/03/2021
# Description: Delete order
@app.route('/orders/<int:id>', methods=['DELETE'])
def remove_order(id):
    Order.delete_order(id)
    response = Response("Order Deleted", status=200, mimetype='application/json')
    return response


# Created by: Manhnd
# Create date: 16h - 14/03/2021
# Description: Get order history
@app.route('/ordersDetail', methods=['GET'])
def get_ordersHistory():
    return jsonify({'Order': OrderDetail.get_all_order_history()})


# Created by: Manhnd
# Create date: 16h - 14/03/2021
# Description: add order + order detail
@app.route('/addOrders', methods=['POST'])
def add_orders():
    request_data = request.get_json()
    idOrder = Order.add_order1(request_data["code"], request_data["userId"],
                               request_data["total"], request_data["createTime"])

    OrderDetail.add_orderDetail(idOrder, request_data["quantity"], request_data["price"],
                                request_data["fruits_id"], request_data["unit"])

    response = Response("addOrder", status=200, mimetype='application/json')
    return response


# Created by: Manhnd
# Create date: 17h30 - 14/03/2021
# Description: add order + order detail
@app.route('/updateOrders/<int:id>', methods=['PUT'])
def update_orders(id):
    request_data = request.get_json()
    Order.update_order(id, request_data["code"], request_data["userId"],
                       request_data["total"], request_data["createTime"])

    listOrderDetail = OrderDetail.get_orderDetail_by_order_id(id)

    for row in listOrderDetail:
        OrderDetail.delete_order_detail_by_orderId(row.get)

    OrderDetail.add_orderDetail(id, request_data["quantity"], request_data["price"],
                                request_data["fruits_id"], request_data["unit"])

    response = Response("addOrder", status=200, mimetype='application/json')
    return response


# Created by: Manhnd
# Create date: 14h - 14/03/2021
# Description: Delete order
@app.route('/deleteOrders/<int:id>', methods=['DELETE'])
def remove_orders(id):
    Order.delete_order(id)
    listOrderDetail = OrderDetail.get_orderDetail_by_order_id(id)
    for row in listOrderDetail:
        OrderDetail.delete_order_detail_by_orderId(row.get)
    response = Response("Orders Deleted", status=200, mimetype='application/json')
    return response


if __name__ == "__main__":
    app.run(port=1234, debug=True)
