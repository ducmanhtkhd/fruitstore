import sqlite3


def main():
    db = sqlite3.connect('FruitStore.db')
    cursor = db.cursor()

    for row in cursor.execute('SELECT * from fruits'):
        print(row)

    for row in cursor.execute('SELECT * from orders'):
        print(row)

    for row in cursor.execute('SELECT * from users'):
        print(row)


if __name__ == '__main__':
    main()
