from dbConnect import *
import json
import sqlite3

db = SQLAlchemy(app)


class OrderDetail(db.Model):
    __tablename__ = 'orderDetail'
    id = db.Column(db.Integer, primary_key=True)
    order_id = db.Column(db.Integer, nullable=False)
    price = db.Column(db.Integer, nullable=False)
    fruits_id = db.Column(db.Integer, nullable=False)
    quantity = db.Column(db.Integer, nullable=False)
    unit = db.Column(db.String(255), nullable=False)

    def json(self):
        return {'id': self.id, 'order_id': self.order_id, 'price': self.price,
                'fruits_id': self.fruits_id, 'quantity': self.quantity, 'unit': self.unit}

    def add_orderDetail(_order_id, _quantity, _price, _fruits_id, _unit):
        new_orderDetail = OrderDetail(order_id=_order_id, quantity=_quantity, price=_price, fruits_id=_fruits_id,
                                      unit=_unit)
        db.session.add(new_orderDetail)
        db.session.commit()

    def get_all_orderDetail():
        return [OrderDetail.json(orders) for orders in OrderDetail.query.all()]

    def get_orderDetail(_id):
        return [OrderDetail.json(OrderDetail.query.filter_by(id=_id).first())]

    def get_orderDetail_by_order_id(_id):
        return OrderDetail.query.filter_by(order_id=_id).first()

    def update_order_detail(_id, _order_id, _quantity, _price, _fruits_id, _unit):
        order_to_update = OrderDetail.query.filter_by(id=_id).first()
        order_to_update.id = _id
        order_to_update.order_id = _order_id
        order_to_update.quantity = _quantity
        order_to_update.price = _price
        order_to_update.fruits_id = _fruits_id
        order_to_update.unit = _unit
        db.session.commit()

    def delete_order_detail(_id):
        OrderDetail.query.filter_by(id=_id).delete()
        db.session.commit()

    def delete_order_detail_by_orderId(_id):
        OrderDetail.query.filter_by(order_id=_id).delete()
        db.session.commit()

    def jsonOrderHistory(self):
        return {'fullName': self.fullName, 'code': self.code,
                'name': self.name, 'quantity': self.quantity, 'unit': self.unit, 'price': self.price,
                'createTime': self.createTime}

    def get_all_order_history():
        listAll = []
        db1 = sqlite3.connect('FruitStore.db')
        cursor = db1.cursor()
        for row in cursor.execute(
                'select users.fullName, ord.code, f.name, detail.quantity, detail.price, ord.createTime from orders ord join orderDetail detail on ord.id = detail.order_id join users users on users.id = ord.userId join fruits f on detail.fruits_id = f.id where users.id =1 '):
            listAll.append(row)

        return json.dumps(listAll)
